from django.shortcuts import render
from django.conf import settings
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from QQLoginTool.QQtool import OAuthQQ

# Create your views here.
from rest_framework_jwt.settings import api_settings

from oauth.models import OAuthQQUser
from oauth.serializers import QQAuthUserSerializer
from oauth.utils import generate_save_user_token


class QQAuthURLView(APIView):
    def get(self, request):
        next = request.query_params.get('next')
        if not next:
            next = '/'
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI,
                        state=next)
        login_url = oauth.get_qq_url()
        "https://graph.qq.com/oauth2.0/authorize?client_id=101474184&response_type=code&state=%2F&redirect_uri=http%3A%2F%2Fwww.meiduo.site%3A8080%2Foauth_callback.html"
        return Response({'login_url': login_url})

class QQAuthUserView(CreateAPIView):
    serializer_class = QQAuthUserSerializer
    def get(self, request):
        # 获取code
        code = request.query_params.get('code')
        if not code:
            return Response({'message': '缺少code'}, status=status.HTTP_400_BAD_REQUEST)
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI)
        try:
            access_token = oauth.get_access_token(code)
            openid = oauth.get_open_id(access_token)
        except Exception as e:
            return Response({'message': e}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        # 查看用户是否已经绑定
        try:
            oauth_user = OAuthQQUser.objects.get(openid=openid)
        except OAuthQQUser.DoesNotExist:
            # 用户没有绑定
            access_token_openid = generate_save_user_token(openid)
            return Response({'access_token': access_token_openid})
        # 用户已经绑定
        user = oauth_user.user
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        return Response({'token': token,
                         'user_id': user.id,
                         'username': user.username})
