from django.conf import settings
from itsdangerous import TimedJSONWebSignatureSerializer as TJS


def generate_save_user_token(openid):
    tjs = TJS(settings.SECRET_KEY, expires_in=300)
    token = tjs.dumps({'openid': openid})
    return token.decode()


def check_save_user_token(openid):
    tjs = TJS(settings.SECRET_KEY, expires_in=300)
    try:
        token = tjs.loads(openid)
    except Exception:
        return None
    return token.get('openid')