import re

from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from .models import User, Address
from django_redis import get_redis_connection
from celery_tasks.email.tasks import send_verify_email

class CreateUserSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(max_length=20, min_length=8, help_text='确认密码', write_only=True)
    sms_code = serializers.CharField(max_length=6, min_length=6, help_text='短信验证码', write_only=True)
    allow = serializers.CharField(help_text='同意协议', write_only=True)
    # 添加token字段
    token = serializers.CharField(help_text='登陆状态token', read_only=True)
    class Meta:
        model = User
        fields = ('id', 'username', 'mobile', 'password', 'password2',
                  'sms_code', 'allow', 'token')
        extra_kwargs = {
            'username': {
                'max_length': 20,
                'min_length': 5,
                'error_messages': {
                    'max_length': '仅允许5-20个字符的用户名',
                    'min_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'max_length': 20,
                'min_length': 8,
                'error_messages': {
                    'max_length': '仅允许8-20个字符的密码',
                    'min_length': '仅允许8-20个字符的密码',
                }
            },
        }

    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号码格式错误')
        return value

    def validate_allow(self, value):
        if value != 'true':
            raise serializers.ValidationError('请同意协议')
        return value

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError('两次密码不一致')
        redis_conn = get_redis_connection('verify')
        real_sms_code = redis_conn.get('sms_code_' + attrs['mobile'])
        if real_sms_code is None:
            raise serializers.ValidationError('短信验证码已过期')
        if attrs['sms_code'] != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')
        return attrs

    def create(self, validated_data):
        del validated_data['password2']
        del validated_data['sms_code']
        del validated_data['allow']

        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token
        return user


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'mobile', 'email', 'email_active')

class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email')
        extra_kwargs = {
            'email': {
                'required': True
            }
        }

    def update(self, instance, validated_data):
        instance.email = validated_data['email']
        instance.save()
        # 发送邮箱验证邮件
        verify_url = instance.generate_verify_email_url()
        send_verify_email.delay(validated_data['email'], verify_url)
        return instance


class UserAddAddressSerializer(serializers.ModelSerializer):
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(required=True, label='省ID')
    city_id = serializers.IntegerField(required=True, label='市ID')
    district_id = serializers.IntegerField(required=True, label='区ID')

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_time', 'update_time')

    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)