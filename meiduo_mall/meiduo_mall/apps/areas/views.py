from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.
from rest_framework_extensions.cache.mixins import CacheResponseMixin

from areas.models import Area
from areas.serializers import AreaSerializer

"""
请求省份数据
请求方式
    GET /areas/
请求参数
    无
返回数据
    JSON
        id	int	是	省份id
        name	str	是	省份名称
"""
class AreasView(ListAPIView, CacheResponseMixin):
    serializer_class = AreaSerializer
    queryset = Area.objects.filter(parent=None)


class AreaView(ListAPIView, CacheResponseMixin):
    serializer_class = AreaSerializer

    def get_queryset(self):
        return Area.objects.filter(parent_id=self.kwargs['parent_id'])