import random

from django_redis import get_redis_connection
from rest_framework.response import Response
from rest_framework.views import APIView

from celery_tasks.sms import tasks as sms_tasks

# Create your views here.

# get sms_codes/(?P<mobile>1[3-9]\d{9})/
# mobile str
# json {'message': 'ok'}
class SMSCodeView(APIView):
    def get(self, request, mobile):
        sms_code_mobile = 'sms_code_' + mobile
        sms_flag_mobile = 'sms_flag_' + mobile

        # 请求频率控制
        redis_conn = get_redis_connection('verify')
        sms_flag = redis_conn.get(sms_flag_mobile)
        if sms_flag:
            return Response({'message': '短信验证码获取过于频繁，请稍后再试'})

        # 生成验证码
        sms_code = '%06d' % random.randint(1, 999999)
        print('sms_code:', sms_code)

        # 保存验证码
        pl = redis_conn.pipeline()
        pl.setex(sms_code_mobile, 300, sms_code)
        pl.setex(sms_flag_mobile, 60, 1)
        pl.execute()

        # 发送验证码
        try:
            sms_tasks.send_sms_code.delay(mobile, sms_code, 5)
        except Exception as e:
            print(e)

        return Response({'message': 'ok'})