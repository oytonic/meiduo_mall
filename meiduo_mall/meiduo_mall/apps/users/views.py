from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView, GenericAPIView, ListAPIView, \
    DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from itsdangerous import TimedJSONWebSignatureSerializer as TJS
from django.conf import settings
from rest_framework.viewsets import ViewSet

from users.serializers import CreateUserSerializer, UserDetailSerializer, EmailSerializer, UserAddAddressSerializer
from .models import User

# Create your views here.

# 用户名查重
# get usernames/(?P<username>\w+)/count/
# username str
# json {'username': username, 'count': count}
class UsernameCountView(APIView):
    def get(self, request, username):
        count = User.objects.filter(username=username).count()
        return Response({'username': username, 'count': count})

# 手机号查重
# get mobiles/(?P<mobile>1[3-9]\d{9})/count/
# mobile str
# json {'mobile': mobile, 'count': count}
class MobileCountView(APIView):
    def get(self, request, mobile):
        count = User.objects.filter(mobile=mobile).count()
        return Response({'mobile': mobile, 'count': count})
# 注册
# post users/
# params
#     id
#     username
#     mobile
#     password
#     password2
#     sms_code
#     allow
# json {'id': id, 'username': username, 'mobile': mobile}
class UserView(CreateAPIView):
    serializer_class = CreateUserSerializer

"""
后端接口设计
请求方式
    GET /user/
请求参数
    无
返回数据
    JSON
        id	int	是	用户id
        username	str	是	用户名
        mobile	str	是	手机号
        email	str	是	email邮箱
        email_active	bool	是	邮箱是否通过验证
"""
class UserDetailView(RetrieveAPIView):
    serializer_class = UserDetailSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user

"""
后端接口设计：
    请求方式
        PUT /email/
    请求参数
        JSON 或 表单
            email	str	是	Email邮箱
    返回数据
        JSON
            id	int	是	用户id
            email	str	是	Email邮箱
"""
class EmailView(UpdateAPIView):
    serializer_class = EmailSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user

"""
后端接口设计：
    请求方式
        GET /email/verification/?token=xxx
    请求参数
        查询字符串参数
            token	str	是	用于验证邮箱的token
    返回数据
        JSON
            message	str	是	验证处理结果
"""
class VerifyEmailView(APIView):
    def get(self, request):
        token = request.query_params.get('token', None)
        if not token:
            return Response({'message': '缺少token'}, status=status.HTTP_400_BAD_REQUEST)
        user = User.check_verify_email_token(token)
        if not user:
            return Response({'message': '验证链接无效'}, status=status.HTTP_400_BAD_REQUEST)
        user.email_active = True
        user.save()
        return Response({'message': 'ok'})

class AddressView(ListAPIView, CreateAPIView, UpdateAPIView, ViewSet, DestroyAPIView):
    serializer_class = UserAddAddressSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.request.user.addresses.filter(is_deleted=False)

    def get_object(self):
        return self.request.user.addresses.get(pk=self.kwargs['pk'])

    @action(methods=['put'], detail=True)
    def status(self, request, pk=None):
        address = self.get_object()
        request.user.defualt_address = address
        request.user.save()
        return Response({'message': 'ok'}, status=status.HTTP_200_OK)

    def destroy(self, reqeust, pk=None):
        address = self.get_object()
        address.is_deleted = True
        address.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=['put'], detail=True)
    def title(self, request, pk=None):
        address = self.get_object()
        address.title = request.data['title']
        address.save()
        return Response({'message': 'ok'}, status=status.HTTP_200_OK)