from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^areas/$', views.AreasView.as_view()),
    url(r'^areas/(?P<parent_id>\d+)/$', views.AreaView.as_view()),
]