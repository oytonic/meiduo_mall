from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from . import views

urlpatterns = [
    url(r'^usernames/(?P<username>\w+)/count/$', views.UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url(r'^users/$', views.UserView.as_view()),
    url(r'^user/$', views.UserDetailView.as_view()),
    url(r'^authorizations/$', obtain_jwt_token),
    url(r'^emails/$', views.EmailView.as_view()),
    url(r'^emails/verification/$', views.VerifyEmailView.as_view()),
    url(r'^addresses/$', views.AddressView.as_view({'get': 'list'})),
    url(r'^addresses/(?P<pk>\d+)/$', views.AddressView.as_view({'put': 'update', 'delete': 'destroy'})),
    url(r'^addresses/(?P<pk>\d+)/status/$', views.AddressView.as_view({'put': 'status'})),
    url(r'^addresses/(?P<pk>\d+)/title/$', views.AddressView.as_view({'put': 'title'}))
]