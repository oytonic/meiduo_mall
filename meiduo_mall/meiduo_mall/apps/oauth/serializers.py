from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from oauth.models import OAuthQQUser
from oauth.utils import check_save_user_token
from users.models import User


class QQAuthUserSerializer(serializers.Serializer):
    access_token = serializers.CharField(write_only=True, label='操作凭证')
    mobile = serializers.RegexField(regex='^1[3-9]\d{9}$', label='手机号')
    password = serializers.CharField(max_length=20, min_length=8, write_only=True, label='密码')
    sms_code = serializers.CharField(max_length=6, min_length=6, write_only=True)
    username = serializers.CharField(max_length=20, label='用户名', help_text='用户名', read_only=True)
    user_id = serializers.IntegerField(label='用户id', read_only=True)
    token = serializers.CharField(max_length=64, read_only=True)

    def validate(self, attrs):
        access_token = attrs['access_token']
        openid = check_save_user_token(access_token)
        if not openid:
            raise serializers.ValidationError('无效的access_token')
        attrs['openid'] = openid
        mobile = attrs['mobile']
        sms_code = attrs['sms_code']
        redis_conn = get_redis_connection('verify')
        real_sms_code = redis_conn.get('sms_code_' + mobile)
        if sms_code != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')

        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            # 用户没有注册
            return attrs

        # 用户已注册
        password = attrs['password']
        if not user.check_password(password):
            raise serializers.ValidationError('密码错误')
        attrs['user'] = user
        return attrs

    def create(self, validated_data):
        user = validated_data.get('user')
        # 若用户没有注册
        if not user:
            # user = User.objects.create(username=validated_data['mobile'],
            #                            password=validated_data['password'],
            #                            mobile=validated_data['mobile'])
            user = User.objects.create(username=validated_data['mobile'],
                                       password=validated_data['password'],
                                       mobile=validated_data['mobile'])
            user.set_password(validated_data['password'])
            user.save()
        OAuthQQUser.objects.create(openid=validated_data['openid'],
                                   user=user)
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token
        user.user_id = user.id
        return user